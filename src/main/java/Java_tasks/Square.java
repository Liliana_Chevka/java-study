package Java_tasks;

public class Square implements Counter {
    private int side;

    public Square(int side) {
        this.side = side;
    }


    @Override
    public String toString() {
        return "Square with value for side - " + " " + side;
    }


    @Override
    public int countThePerimeter() {
        return side * 4;
    }
}
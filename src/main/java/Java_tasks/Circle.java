package Java_tasks;


public class Circle implements Counter {
    private int r;

    public Circle(int r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle with value for radius - " + r;
    }


    @Override
    public int countThePerimeter() {
        return (int) (2 * Math.PI * r);
    }
}
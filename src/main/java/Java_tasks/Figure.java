package Java_tasks;

import java.util.Scanner;

public class Figure {

    public static void main(String[] args) {

        System.out.println("If you want to calculate the perimeter of a geometric figure enter:\n" +
                "1 for Square\n" +
                "2 for Rectangle\n" +
                "3 for Triangle\n" +
                "4 for Circle\n");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        switch (number) {

            case 1:
                Counter count1 = countSquare(scan);
                System.out.println(count1);
                System.out.println("The perimeter is : " + count1.countThePerimeter());
                break;

            case 2:
                Counter count2 = countRectangle(scan);
                System.out.println(count2);
                System.out.println("The perimeter is : " + count2.countThePerimeter());

                break;

            case 3:
                Counter count3 = countTriangle(scan);
                System.out.println(count3);
                System.out.println("The perimeter is : " + count3.countThePerimeter());
                break;
            case 4:
                Counter count4 = countCircle(scan);
                System.out.println(count4);
                System.out.println("The perimeter is : " + count4.countThePerimeter());
                break;
            default:
                System.out.println("You entered an invalid number");
        }
    }

    static Square countSquare(Scanner scan) {

        System.out.println("Enter the value for side");
        int side = scan.nextInt();
        return new Square(side);

    }

    static Rectangle countRectangle(Scanner scan) {

        System.out.println("Enter the value for side1");
        int side1 = scan.nextInt();
        System.out.println("Enter the value for side2");
        int side2 = scan.nextInt();
        return new Rectangle(side1, side2);

    }

    static Triangle countTriangle(Scanner scan) {

        System.out.println("Enter the value for the first side of the triangle");
        int side1t = scan.nextInt();
        System.out.println("Enter the value for the second side of the triangle");
        int side2t = scan.nextInt();
        System.out.println("Enter the value for the third side of the  triangle");
        int side3t = scan.nextInt();
        return new Triangle(side1t, side2t, side3t);

    }

    static Circle countCircle(Scanner scan) {

        System.out.println("Enter the value of the circle's radius");
        int r = scan.nextInt();
        return new Circle(r);

    }
}
package Java_tasks;

public class Linear {

    private double a;
    private double b;
    private double c;


    public Linear(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double solver() {
        double x = (c - b) / a;

        return x;
    }
}
package Java_tasks;

public class Rectangle implements Counter {
    private int side1;
    private int side2;

    public Rectangle(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public String toString() {
        return "Rectangle with values for sides - " + side1 + "," + side2;
    }


    @Override
    public int countThePerimeter() {
        return 2 * (side1 + side2);
    }
}
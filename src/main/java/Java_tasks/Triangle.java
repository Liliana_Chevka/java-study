package Java_tasks;

public class Triangle implements Counter {
    private int side1t;
    private int side2t;
    private int side3t;

    public Triangle(int side1t, int side2t, int side3t) {
        this.side1t = side1t;
        this.side2t = side2t;
        this.side3t = side3t;
    }

    @Override
    public String toString() {
        return "Triangle with values for sides - " + side1t + "," + side2t + "," + side3t;
    }


    @Override
    public int countThePerimeter() {
        return side1t + side2t + side3t;
    }
}